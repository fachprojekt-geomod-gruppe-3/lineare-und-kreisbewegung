using LinearAlgebra

"""Calculates the linear route from point `a` (inclusive) to point `b` (only inclusive if `feed`
divides the distance between the points evenly) in discrete steps of length `feed`. Returns a
Union{Vector{Vector{Float64}}, Nothing}. Nothing is returned if `a` and `b` are the same
point or extremely close to each other."""
function linear_motion(a::Vector{Float64}, b::Vector{Float64}, feed::Float64)
    vec = b - a
    distance = norm(vec)

    if distance < eps()
        nothing
    else
        #= Discretise motion into lines of length `feed`. =#
        step_count = div(distance, feed)
        step_vec = vec / distance * feed

        map(i -> a + i * step_vec, 0:step_count)
    end
end

"""Calculates the circular route from point `a` (inclusive) to point `b` (only inclusive if `feed`
divides the arc length between the points evenly) with circle center `cc` around the `axis` in the
specified `direction` in discrete steps of length `feed`. Returns a Union{Vector{Vector{Float64}}, Nothing}.
Nothing is returned if the radius of the circle is extremely small."""
function circular_motion(a::Vector{Float64}, b::Vector{Float64}, cc::Vector{Float64},
axis::Char, direction::Char, feed::Float64)
    n = indexin(axis, ['X', 'Y', 'Z'])[1]

    if abs(cc[n] - a[n]) > eps() || abs(cc[n] - b[n]) > eps()
        throw("Points are not all on a shared plane perpendicular to axis $axis.")
    end

    vec_a = a - cc
    vec_b = b - cc
    radius = norm(vec_a)

    #= Note: eps() is too strict here. =#
    if abs(radius - norm(vec_b)) > 0.01
        throw("Points a and b are not equally far away from the circle center.")
    end

    if radius < eps()
        nothing
    else
        #= Calculate angles. =#
        k = n == 1 ? 2 : 1
        m = n == 2 ? 3 : k+1
        angle_a = atan(vec_a[m], vec_a[k])
        angle_b = atan(vec_b[m], vec_b[k])

        total_angle = abs(angle_b - angle_a)
        direction_sign = direction == '-' ? -1.0 : 1.0

        #= Take the opposite angle if either rotating around y axis or
            if the direction does not match. =#
        if xor(n == 2, sign(angle_b - angle_a) != direction_sign)
            total_angle = 2pi - total_angle
        end

        #= Discretise motion into arcs of length `feed`. =#
        step_angle = direction_sign / radius * feed
        step_count = div(total_angle, abs(step_angle))
        rot_mat = [rot_mat_x, rot_mat_y, rot_mat_z][n]

        map(i -> cc + rot_mat(i * step_angle) * vec_a, 0:step_count)
    end
end


#= Rotation matrices =#

function rot_mat_x(angle)
    c = cos(angle)
    s = sin(angle)

    [1 0 0; 0 c -s; 0 s c]
end

function rot_mat_y(angle)
    c = cos(angle)
    s = sin(angle)

    [c 0 s; 0 1 0; -s 0 c]
end

function rot_mat_z(angle)
    c = cos(angle)
    s = sin(angle)

    [c -s 0; s c 0; 0 0 1]
end
