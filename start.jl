include("calc_steps.jl")
include("renderer.jl")

if length(ARGS) == 1
    render(ARGS[1])
elseif length(ARGS) == 2
    calc_steps([0.0, 0.0, 0.0], ARGS[1], ARGS[2])
    render(ARGS[2])
end
