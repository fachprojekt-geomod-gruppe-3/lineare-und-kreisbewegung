# Werkzeugbewegung

Liest vom Klartext-Parser ausgegebene Befehle und berechnet die Zwischenschritte der Werkzeugbewegung.
Die Ausgabe erfolgt als CSV-Datei.
Rendert außerdem die berechneten Positionen als Punktwolke.

Benutzung:
- Berechnung, Ausgabe und Rendern: julia start.jl INPUT.json OUTPUT.csv
- Rendern einer bereits erstellten Ausgabe: julia start.jl OUTPUT.csv
