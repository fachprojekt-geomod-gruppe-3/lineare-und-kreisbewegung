const vertex_shader_source_unlit_test = 
"""
    #version 330
    uniform mat4 scalingMatrix;
    uniform mat4 zRotationMatrix;
    uniform mat4 yRotationMatrix;
    uniform mat4 xRotationMatrix;
    uniform vec4 posTransform;

    in vec3 position;

    void main() {
        gl_Position = scalingMatrix * ((xRotationMatrix * yRotationMatrix * zRotationMatrix * vec4(position, 1.0)) + posTransform);
    }
"""

const fragment_shader_source_unlit_test = 
"""
    #version 330
    out vec4 fragColor;

    void main(void) {
        fragColor = vec4(1.0, 1.0, 1.0, 0.0);
    }
"""

#start copy of shaders.py
const vertex_shader_source_unlit = 
"""
    #version 330
    uniform mat4 modelViewProj;
    in vec3 position;
    in vec3 color;
    out vec4 vertexColor;

    void main() {
        gl_Position = modelViewProj * vec4(position, 1.0);
        vertexColor = vec4(color, 1);
    }
"""

const fragment_shader_source_unlit = 
"""
    #version 330
    in vec4 vertexColor;
    out vec4 fragColor;

    void main(void) {
        fragColor = vertexColor;
    }
"""
#end
