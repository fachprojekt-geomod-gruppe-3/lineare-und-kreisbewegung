import GLFW
using ModernGL

include("glUtil.jl")
include("shaders.jl")

#Variables to controle camera position
global keysPressed = []
#Variable to controle zoom
global scalingFactor = 0.007 
#Variables to maniputale rotation use deg2rad() when manipulation via call
global zRotationAngle = 0
global yRotationAngle = 0
global xRotationAngle = 0

global cameraX = 0
global cameraY = 0
global mouseX = 0
global mouseY = 0

function render(csv_path::String)
    #Data sollte am besten so aussehen: P1,P2 ,P2,P3, P3,P4 um die linien einfach mit OpenGL darstellen zu können
    data = parseCSV(csv_path::String)

    #create Window
    GLFW.Init()
    window = GLFW.Window(name="Machining Simulation", resolution=(1200,800));

    vao = glGenVertexArray()
    glBindVertexArray(vao)
    vbo = glGenBuffer()
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW)

    vertexShader = createShader(vertex_shader_source_unlit_test, GL_VERTEX_SHADER)
    fragmentShader = createShader(fragment_shader_source_unlit_test, GL_FRAGMENT_SHADER)
    program = createShaderProgram(vertexShader, fragmentShader)
    glUseProgram(program)
    #glViewport(convert(GLint, -500), convert(GLint, -500), convert(GLsizei, 500), convert(GLsizei, 500))

    #inizialize shader attribute "position"
    positionAttribute = glGetAttribLocation(program, "position");
    glEnableVertexAttribArray(positionAttribute)
    glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, 0, C_NULL)

    #Input handling setup
    GLFW.SetScrollCallback(window, scrollCallback)
    GLFW.SetKeyCallback(window, keyboardCallback)
    GLFW.SetMouseButtonCallback(window, mouseClickCallback)


    dataPointsCount = length(data)/3
    #render content
    while !GLFW.WindowShouldClose(window)

        inputHandling(window)
        updateVertexShader(program)

        #Draw
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT)
        glDrawArrays(GL_POINTS, 0, dataPointsCount)
        GLFW.SwapBuffers(window)
        GLFW.PollEvents()
    end

    GLFW.Terminate()
end

#parse CSV data into GLfloat
function parseCSV(csv_path::String)
    io = open(csv_path)
    lines = readlines(io)

    count = 0
    data = GLfloat[]
    time = GLfloat[]
    for l in lines
        splits = split(l, ",")
        if splits[1] == "tool"
            continue
        end
        for s in splits
            if count < 3
                push!(data, parse(GLfloat, s))
                count += 1
                continue
            end
            push!(time, parse(GLfloat, s))
            count = 0
        end
    end

    #Do something with timestamps
    return data
end

function inputHandling(window)
    
    global xRotationAngle
    global yRotationAngle
    global zRotationAngle 
    global cameraX
    global cameraY
    global mouseX
    global mouseY
    
    keyboardCameraOffset = 0.05;
    mouseCameraOffset = 0.1;

    for keys in keysPressed
        if keys == GLFW.MOUSE_BUTTON_RIGHT
            mouseNewX = GLFW.GetCursorPos(window)[1]
            mouseNewY = GLFW.GetCursorPos(window)[2]
            if mouseNewX < mouseX
                zRotationAngle -= deg2rad(1)
            elseif mouseNewX > mouseX
                zRotationAngle += deg2rad(1)
            end
            if mouseNewY < mouseY
                xRotationAngle -= deg2rad(1)
            elseif mouseNewY > mouseY
                xRotationAngle += deg2rad(1)
            end
            mouseX = mouseNewX
            mouseY = mouseNewY
            
        elseif keys == GLFW.MOUSE_BUTTON_LEFT
            mouseNewX = GLFW.GetCursorPos(window)[1]
            mouseNewY = GLFW.GetCursorPos(window)[2]
            xCursorOffset = mouseNewX - mouseX
            if xCursorOffset < 0
                cameraX += mouseCameraOffset * (-xCursorOffset)
            elseif xCursorOffset > 0
                cameraX -= mouseCameraOffset * (xCursorOffset)
            end
            yCursorOffset = mouseNewY - mouseY
            if yCursorOffset < 0
                cameraY -= mouseCameraOffset * (-yCursorOffset)
            elseif yCursorOffset > 0
                cameraY += mouseCameraOffset * (yCursorOffset)
            end
            mouseX = mouseNewX
            mouseY = mouseNewY
        end
        
        #TODO scaling vom cameraOffeset auch bei Tastatureingabe
        if keys == GLFW.KEY_RIGHT || keys == GLFW.KEY_D
            cameraX += keyboardCameraOffset
        elseif keys == GLFW.KEY_LEFT || keys == GLFW.KEY_A
            cameraX -= keyboardCameraOffset
        end
        if keys == GLFW.KEY_UP || keys == GLFW.KEY_W
            cameraY += keyboardCameraOffset
        elseif keys == GLFW.KEY_DOWN || keys == GLFW.KEY_S
            cameraY -= keyboardCameraOffset
        end
    end
end

function updateVertexShader(shaderProgram)

    global xRotationAngle
    global yRotationAngle
    global zRotationAngle
    global scalingFactor

    #update shader uniform "scalingMatrix" 
    scalingMatrix = GLfloat[[scalingFactor 0 0 0]; [0 scalingFactor 0 0]; [0 0 scalingFactor 0]; [0 0 0 1]]
    transformLocation = glGetUniformLocation(shaderProgram, "scalingMatrix")
    glUniformMatrix4fv(transformLocation, 1, GL_FALSE, scalingMatrix)
    
    #update shader uniform "xRotationMatrix"
    xRotationMatrix = GLfloat[[1 0 0 0]; [0 cos(xRotationAngle) -sin(xRotationAngle) 0]; [0 sin(xRotationAngle) cos(xRotationAngle) 0]; [0 0 0 1]]
    transformLocation = glGetUniformLocation(shaderProgram, "xRotationMatrix")
    glUniformMatrix4fv(transformLocation, 1, GL_FALSE, xRotationMatrix)
    #update shader uniform "yRotationMatrix"
    yRotationMatrix = GLfloat[[cos(yRotationAngle) 0 sin(yRotationAngle) 0]; [0 1 0 0]; [-sin(yRotationAngle) 0 cos(yRotationAngle) 0]; [0 0 0 1]]
    transformLocation = glGetUniformLocation(shaderProgram, "yRotationMatrix")
    glUniformMatrix4fv(transformLocation, 1, GL_FALSE, yRotationMatrix)
    #update shader uniform "zRotationMatrix"
    zRotationMatrix = GLfloat[[cos(zRotationAngle) -sin(zRotationAngle) 0 0]; [sin(zRotationAngle) cos(zRotationAngle) 0 0]; [0 0 1 0]; [0 0 0 1]]
    transformLocation = glGetUniformLocation(shaderProgram, "zRotationMatrix")
    glUniformMatrix4fv(transformLocation, 1, GL_FALSE, zRotationMatrix)

    #update shader uniform "posTransform"
    posTransform = GLfloat[-cameraX -cameraY 0 0]
    transformLocation = glGetUniformLocation(shaderProgram, "posTransform")
    glUniform4fv(transformLocation, 1, posTransform)
end

#Input handling callback functions
function scrollCallback(window, xOffset, yOffset)
    global scalingFactor
    if yOffset > 0
        scalingFactor += 0.0005
    elseif yOffset < 0
        scalingFactor -= 0.0005
    end
end

function keyboardCallback(window, key, scanCode, action, mods)
    global keysPressed
    if action == GLFW.PRESS
        push!(keysPressed, key)
    elseif action == GLFW.RELEASE
        filter!(f->f != key, keysPressed)
    end
end

function mouseClickCallback(window, button, action, mods)
    global keysPressed
    global mouseX
    global mouseY
    if action == GLFW.PRESS
        push!(keysPressed, button)
        mouseX = GLFW.GetCursorPos(window)[1]
        mouseY = GLFW.GetCursorPos(window)[2]
    elseif action == GLFW.RELEASE
        filter!(f->f != button, keysPressed)
    end
end


#debug printing for Data
function printData(data, countlimit::Int)
    count = 0
    countXYZ = 0
    for i in data
        if count < countlimit
            print(i)
            count += 1
            if countXYZ == 0
                print(", ")
                countXYZ += 1
            elseif countXYZ == 1
                print(", ")
                countXYZ += 1
            elseif countXYZ == 2
                println(",")
                countXYZ = 0
            end
        end
    end
end
