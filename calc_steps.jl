using LinearAlgebra
import JSON
include("bewegung.jl")

"""Stores properties of a tool."""
struct Tool
    diameter::Float64
    length::Float64
    corner_radius::Float64
    blade_count::UInt
end

"""Stores the current simulation state."""
mutable struct State
    position::Vector{Float64}
    m91_position::Union{Vector{Float64},Nothing}
    tool::Union{Tool,Nothing}
    feed_rate::Union{Float64,Nothing}
    spindle_speed::Union{Float64,Nothing}
    spindle_axis::Union{Char,Nothing}
    circle_center::Union{Vector{Float64},Nothing}

    State(position) = new(position, nothing, nothing, nothing, nothing, nothing, nothing)
end

#= Constants =#
const max_feed_rate = 2000.0
const tools = [Tool(5.0, 30.0, 0.0, 2), Tool(2.0, 30.0, 0.0, 2)]
const m91_zero = [0.0, 0.0, 0.0]

"""Reads instructions from file at `json_path` and writes discretised tool positions into file at `csv_path`."""
function calc_steps(start_position::Vector{Float64}, json_path::String, csv_path::String)
    #= Initialize values. =#
    state = State(start_position)
    previous_position = nothing
    previous_feed_rate = nothing

    #= Open files. =#
    json = JSON.parsefile(json_path)
    csv = open(csv_path, "w")

    #= Handle instructions. =#
    for instruction in json
        base_instruction = instruction["base_instruction"]
        modifiers = something(instruction["modifiers"], [])

        if base_instruction != nothing
            if haskey(base_instruction, "Linear")
                #= Write positions of linear motion into CSV file. =#
                positions = linear!(state, base_instruction["Linear"], modifiers)
                write_positions(positions, csv, state, previous_position, previous_feed_rate)

                #= Update values. =#
                previous_position = positions[end]
                previous_feed_rate = if base_instruction["Linear"]["feed_rate"] == "MAX"
                    max_feed_rate
                else
                    state.feed_rate
                end
            elseif haskey(base_instruction, "CircleCenter")
                #= Set circle center.=#
                circle_center!(state, base_instruction["CircleCenter"])
            elseif haskey(base_instruction, "Circle")
                #= Write positions of linear motion into CSV file. =#
                positions = circle!(state, base_instruction["Circle"], modifiers)
                write_positions(positions, csv, state, previous_position, previous_feed_rate)

                #= Update values. =#
                previous_position = positions[end]
                previous_feed_rate = if base_instruction["Circle"]["feed_rate"] == "MAX"
                    max_feed_rate
                else
                    state.feed_rate
                end
            elseif haskey(base_instruction, "ToolCall")
                #= Perform a tool call and write tool number into CSV file. =#
                toolcall!(state, base_instruction["ToolCall"])
                n = base_instruction["ToolCall"]["tool_number"]
                write(csv, "tool,$n\n")
            end
        end
    end

    #= Write final position into CSV file and close it. =#
    write_positions([state.position], csv, state, previous_position, previous_feed_rate)
    close(csv)
end

"""Write `positions` along with time information to `csv`. Needs current `state`,
the `previous_position` and the `previous_feed_rate` for calculations."""
function write_positions(positions::Vector{Vector{Float64}}, csv::IO, state::State,
previous_position::Union{Vector{Float64},Nothing}, previous_feed_rate::Union{Float64,Nothing})
    for i in eachindex(positions)
        position = positions[i]

        if position != previous_position
            #= Extract coordinates. =#
            x = position[1]; y = position[2]; z = position[3]

            #= Calculate time in milliseconds. =#
            t = 0.0
            if previous_position != nothing
                if i == 1
                    if previous_feed_rate != nothing
                        t = 60_000 * norm(position - previous_position) / previous_feed_rate
                    end
                else
                    if state.spindle_speed != nothing && state.tool != nothing
                        t = 60_000 / (state.spindle_speed * state.tool.blade_count)
                    end
                end
            end

            #= Write position to `csv` and update `previous_position`. =#
            write(csv, "$x,$y,$z,$t\n")
            previous_position = position
        end
    end
end

"""Executes a linear movement instruction."""
function linear!(state::State, instruction, modifiers)
    #= Check modifiers. =#
    use_m91_position = 91 in modifiers

    #= Determine target position. =#
    target = if use_m91_position && state.m91_position != nothing
        get_coords(state.m91_position, instruction)
    else
        get_coords(state.position, instruction)
    end

    #= Use m91_zero as offset if necessary. =#
    if use_m91_position
        target += m91_zero
    end

    #= Discretise motion if possible. =#
    feed = get_feed!(state, instruction)
    positions = if feed != nothing
        something(linear_motion(state.position, target, feed), [state.position])
    else
        [state.position]
    end

    #= Update position(s). =#
    state.position = target
    if use_m91_position
        state.m91_position = target - m91_zero
    end

    positions
end

"""Executes an instruction defining the center of a circular movement."""
function circle_center!(state::State, instruction)
    #= Update circle center position. =#
    state.circle_center = get_coords(state.position, instruction)
end

"""Executes a circular movement instruction."""
function circle!(state::State, instruction, modifiers)
    #= Check modifiers. =#
    use_m91_position = 91 in modifiers

    #= Determine target position. =#
    target = if use_m91_position && state.m91_position != nothing
        get_coords(state.m91_position, instruction)
    else
        get_coords(state.position, instruction)
    end

    #= Use m91_zero as offset if necessary. =#
    if use_m91_position
        target += m91_zero
    end

    #= Check if necessary parameters have been defined. =#
    if state.circle_center === nothing
        throw("ERROR: No circle center defined.")
    elseif state.spindle_axis === nothing
        throw("ERROR: No spindle axis defined.")
    end

    #= Discretise motion if possible. =#
    feed = get_feed!(state, instruction)
    positions = if feed != nothing
        something(circular_motion(state.position, target, state.circle_center,
            state.spindle_axis, instruction["direction"][1], feed), [state.position])
    else
        [state.position]
    end

    #= Update position(s). =#
    state.position = target
    if use_m91_position
        state.m91_position = target - m91_zero
    end

    positions
end

"""Executes an instruction that calls a tool."""
function toolcall!(state::State, instruction)
    #= Update tool and other parameters. =#
    state.tool = tools[instruction["tool_number"]]
    state.spindle_axis = instruction["spindle_axis"][1]
    if instruction["spindle_speed"] != nothing
        state.spindle_speed = instruction["spindle_speed"]
    end
end

"""Calculates coordinates incrementally based on the old coordinates."""
function get_coords(old_coords::Vector{Float64}, instruction)
    new_coords = map(c -> instruction["coordinates"][c], ["x", "y", "z"])
    map(something, new_coords, old_coords)
end

"""Calculates the feed of the current instruction and potentially updates the feed_rate."""
function get_feed!(state::State, instruction)
    feed_rate = if instruction["feed_rate"] == "MAX"
        max_feed_rate
    else
        #= Update feed rate. =#
        if instruction["feed_rate"] != nothing && haskey(instruction["feed_rate"], "Decimal")
            state.feed_rate = instruction["feed_rate"]["Decimal"]
        end

        state.feed_rate
    end

    #= Return feed if all necessary parameters are defined. =#
    if feed_rate != nothing && state.tool != nothing && state.spindle_speed != nothing
        feed_rate / (state.spindle_speed * state.tool.blade_count)
    else
        nothing
    end
end
